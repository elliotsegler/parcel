# the parcel requirements
-r requirements.txt

# stuff for testing
mock
nose
unittest2
coverage==3.5.3

-e git+http://github.com/kennethreitz/envoy.git#egg=envoy
