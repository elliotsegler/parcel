# -*- coding: utf-8 -*-
import os.path

from fabric.api import settings, run, cd, lcd, put, get, local, env, with_settings
from fabric.colors import green

from .. import versions
from .. import distro
from .. import tools
from .. import defaults
from .deploy import Deployment

class gunicorn(Deployment):
    '''
    Gunicorn + supervisord deployment model
    '''

    def add_supervisord_gunicorn_service(self, program_name, user=None, service_name=None, env_vars=None,
                                         app_wsgi=None, app_module='application'):
        '''
        Adds a gunicorn service to supervisord

        :param program_name: The name of the program deployed under supervisord
        :param user: The user the program/service should be running under (Optional)
        :param service_name: The name of the service if different from the program name (Optional)
        :param env_vars: Dict of environment variables to be added to the supervisord config (Optional)
        :param app_wsgi:
        :param app_module:
        :return:
        '''

        if env_vars is None:
            env_vars = {}

        if app_wsgi is None:
            app_wsgi = "%s.wsgi" % program_name

        # if we didn't set a user, pull it from the env
        user = user or env.user

        # if we didn't get a service_name, set it to the program name
        service_name = service_name or program_name

        # write out our gunicorn config file
        self.write_gunicorn_file(program_name=program_name, user=user, group=user, service_name=service_name)

        # build the supervisord config file
        # gunicorn myproject.wsgi:application app.wsgi:application
        config = """[%s:%s]
command = "%s/bin/gunicorn_start --config /etc/gunicorn/%s_%s.settings %s:%s"
user = "%s"
stdout_logfile = "/var/log/%s/%s.log"
redirect_stderr = true
""" % (program_name, service_name, self.venv_root, program_name, service_name, app_wsgi, app_module, user,
       program_name, service_name)

        # add any environment variables we want to pass in
        for key,val in env_vars.items():
            config = "%s%s = \"%s\"\n" % (config, key, val)

        # Write the file out
        self.add_data_to_root_fs(config, "etc/supervisor/conf.d/%s_%s.conf" % (program_name, service_name))

        # add the postinstall lines
        self.add_postinst(['/etc/init.d/supervisor stop', 'sleep 1', '/etc/init.d/supervisor start'])

        # add the prerm lines
        self.add_prerm(['supervisorctl stop %s' % program_name])

        # add the supervisor and gunicorn install dependencies
        self.run_deps.append('supervisor')
        self.run_deps.append('gunicorn')

        # also in postinst is to start this app
        self.add_postinst(['supervisorctl start %s' % program_name])

    def write_gunicorn_file(self, program_name, user=None, group=None, service_name=None, bind='127.0.0.1:8000'):
        '''
        Writes out a gunicorn python-based settings file

        Writes the file out to /etc/gunicorn/program_name.settings

        :param program_name: The name of the program deployed under supervisord
        :param user: The user the program/service should be running under (Optional)
        :param group: The group the program/service should be running under (Optional)
        :param service_name: The name of the service if different from the program name (Optional)
        :param bind: A bind string specifying what the server should listen to (defaults to 127.0.0.1:8000)
        :return:
        '''

        # if we didn't set a user, pull it from the env
        user = user or env.user
        group = group or user

        service_name = service_name or program_name

        data = """
import multiprocessing

## clamp Lambda function
## e.g: clamp(n, 7, 42) will return n so long as it is greater than 7
## and less than 42, otherwise it will return either the min or max
clamp = lambda n, minn, maxn: max(min(maxn, n), minn)

bind = "%s"

user = "%s"
group = "%s"

workers = multiprocessing.cpu_count() * 2 + 1
backlog = clamp(workers * 20, 64, 2048)

max_requests = 0
timeout = 30

access_logfile = "/var/log/%s/%s_gunicorn_access.log"
error_logfile = "/var/log/%s/%s_gunicorn_error.log"
log_level = "debug"
""" % (bind, user, group, program_name, service_name, program_name, service_name)
        self.add_data_to_root_fs(data, 'etc/gunicorn/%s_%s.settings' % (program_name, service_name))


