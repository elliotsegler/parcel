"""
Context manager to build a temporary path
"""

import os
import tempfile
import uuid

class TempDir(object):
    def __init__(self, create=True):
        self.path = os.path.join("/",tempfile.gettempprefix(),str(uuid.uuid4()))
        self.create = create

    def __enter__(self):
        if self.create:
            os.makedirs(self.path)

            self.pwd = os.getcwd()
            os.chdir(self.path)

        return self

    def __exit__(self, *args, **kwargs):
        if self.create:
            os.chdir(self.pwd)
        os.system('rm -rf "{0.path}"'.format(self))
        
