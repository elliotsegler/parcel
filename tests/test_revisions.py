import unittest
import random
import os
from fabric.api import hide

from parcel.revisions import Hg, Git, repo, GitException

from repomanager import RepoTestManager, DEFAULT_REVISIONS, random_data
from tempdir import TempDir

class MercurialRepo(RepoTestManager):
    """Mocked up mercurial repository for testing as a context manager"""
    def __init__(self, path=None, hg=None, revisions=None, clone=None):
        return RepoTestManager.__init__(self, path, hg or "hg", revisions, clone)

class MercurialTestSuite(unittest.TestCase):
    """parcel.revisions.Hg test cases."""

    def test_hg_instantiate(self):
        with MercurialRepo() as repo:
            hg = Hg(repo.path)

    def test_hg_logs(self):
        revisions = DEFAULT_REVISIONS + [
            ('Third custom checkin', {
                    'blah-blah.txt': random_data(random.randint(1024,32000))})]
        set_keys = ('date', 'changeset', 'user', 'summary')
        opt_keys = ('tag', )

        with MercurialRepo(revisions=revisions) as repo:
            logs = Hg(repo.path).logs()
            self.assertEquals(len(logs), len(revisions))
            for log in logs:
                # compulsory keys
                for key in set_keys:
                    self.assertIn(key, log)

                # optional keys
                for key in set(log.keys()).difference(set(set_keys)):
                    self.assertIn(key, opt_keys)

    def test_hg_branch(self):
        with MercurialRepo() as repo:
            self.assertEquals(Hg(repo.path).branch(), 'default')
            
    def test_hg_log(self):
        revisions = DEFAULT_REVISIONS + [
            ('Third custom checkin', {
                    'blah-blah.txt': random_data(random.randint(1024,32000))})]
        set_keys = ('date', 'changeset', 'user', 'summary')
        opt_keys = ('tag', )

        with MercurialRepo(revisions=revisions) as repo:
            log = Hg(repo.path).log()
            # compulsory keys
            for key in set_keys:
                self.assertIn(key, log)

            # optional keys
            for key in set(log.keys()).difference(set(set_keys)):
                self.assertIn(key, opt_keys)

            self.assertEquals(log['summary'], 'Third custom checkin')

    def test_hg_update(self):
        with MercurialRepo() as repo:
            self.assertEquals( Hg(repo.path).update(),
                               {'updated': 0,
                                'removed': 0,
                                'merged': 0,
                                'unresolved': 0})
    
    def test_hg_pull(self):
        with MercurialRepo() as parent:
            with MercurialRepo(clone=parent.path,revisions=[]) as repo:
                hg = Hg(repo.path)
                result = hg.pull()
                
                self.assertIn(parent.path, result[0])

    def test_hg_describe(self):
        with MercurialRepo() as repo:
            description = Hg(repo.path).describe()

            # eg null-2-d63d252639deF
            self.assertTrue(description.startswith('null-2-'))
            self.assertEquals(len(description), 19)

    def test_hg_clone(self):
        revisions = DEFAULT_REVISIONS + [
            ('Third custom checkin', {
                    'check.dat': random_data(1024)})]
        with MercurialRepo(revisions=revisions) as source:
            with TempDir() as dest:
                hg = Hg(dest.path)
                hg.clone(source.path)

                dirs = os.listdir(dest.path)
                self.assertIn('check.dat', dirs)
                

class GitRepo(RepoTestManager):
    """Mocked up mercurial repository for testing as a context manager"""
    def __init__(self, path=None, git=None, revisions=None, clone=None):
        return RepoTestManager.__init__(self, path, git or "git", revisions, clone)

    def tag(self, tagname, message=None, annotated=True):
        message = message if message is None else "{0} committed".format(tagname)
        r = self.run('{0.bin} tag -a {1} -m "{2}"'.format(self, tagname, message))
        assert r.status_code==0

    def checkout(self, tag):
        r = self.run('{0.bin} checkout {1}'.format(self, tag))
        assert r.status_code==0

class GitTestSuite(unittest.TestCase):
    """parcel.revisions.Git test cases."""

    def test_git_instantiate(self):
        with GitRepo() as repo:
            Git(repo.path)

    def test_git_branch(self):
        with GitRepo() as repo:
            self.assertEquals(Git(repo.path).branch(), 'master')

    def test_git_log(self):
        with GitRepo() as repo:
            log = Git(repo.path).log()
            self.assertEquals(len(log), len(DEFAULT_REVISIONS))
            for cs in log:
                # changeset hash
                self.assertEquals(len(cs['changeset']), 40)
                self.assertNotIn(False, [x in "0123456789abcdef" for x in cs['changeset']])
                
                self.assertIn('date', cs)
                self.assertIn('summary', cs)
                self.assertIn('author', cs)

                self.assertEquals(log[-1]['summary'], DEFAULT_REVISIONS[0][0])
                self.assertEquals(log[-2]['summary'], DEFAULT_REVISIONS[1][0])

                self.assertIn('@',cs['author'])
                self.assertEqual(cs['date'].count(':'), 2)

    def test_git_pull(self):
        with GitRepo() as repo:
            with GitRepo(clone=repo.path) as inner:
                result = Git(inner.path).pull()
                self.assertIn('Already up-to-date.', result)

                # make upstream mods
                repo.commit_changes('checkin upstream',{'newfile.dat': random_data(1024) })

                # pull
                result = Git(inner.path).pull()
                self.assertIn('1 file changed', result)
                self.assertIn('1 insertion', result)
                self.assertIn('newfile.dat', result)

    def test_git_checkout(self):
        with GitRepo() as repo:
            git = Git(repo.path)
            git.checkout("master")

            # latest file is present
            self.assertIn('new file with space.exe', os.listdir(repo.path))

            # go back
            tag = git.log()[1]['changeset']
            git.checkout(tag)
            
            # the file is gone
            self.assertNotIn('new file with space.exe', os.listdir(repo.path))
           
    def test_git_clone(self):
        revisions = DEFAULT_REVISIONS + [
            ('Third custom checkin', {
                    'check.dat': random_data(1024)})]
        with GitRepo(revisions=revisions) as source:
            with TempDir() as dest:
                git = Git(dest.path)
                git.clone(source.path)

                dirs = os.listdir(dest.path)
                self.assertIn('check.dat', dirs)
                
    def test_git_describe(self):
        with GitRepo() as repo:
            repo.tag("v1.0.3")
            result = Git(repo.path).describe()
            self.assertEquals("v1.0.3", result)

            repo.commit_changes('checkin upstream',{'newfile.dat': random_data(1024) })
            result = Git(repo.path).describe()
            self.assertTrue(result.startswith("v1.0.3-1-g"))

            # not on a tag, should fail
            with self.assertRaises(GitException):
                with hide('warnings'):
                    result = Git(repo.path).describe(force_tag=True)

            # move to tag and should pass
            repo.checkout("v1.0.3")
            result = Git(repo.path).describe(force_tag=True)
            self.assertEquals(result, "v1.0.3")

class repoTestSuite(unittest.TestCase):
    """parcel.revisions.repo test cases."""
    def test_repo_hg(self):
        with MercurialRepo() as hg:
            r = repo(hg.path)
            self.assertEquals(r.__class__, Hg)

    def test_repo_git(self):
        with GitRepo() as git:
            r = repo(git.path)
            self.assertEquals(r.__class__, Git)

    def test_repo_recurse(self):
        with MercurialRepo() as hg:
            path = os.path.join(hg.path,"dir1","dir2","dir3")
            os.makedirs(path)
            r = repo(path)
            self.assertEquals(r.__class__, Hg)
            
        
