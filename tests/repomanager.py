"""A context manager to use with the test code for parcel.revisions"""

import os
import tempfile
import uuid
import envoy
import random

def random_data(size):
    return "".join(
        [ random.choice(r"\n\r abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-+=[]{}\|`~,.<>/?;:'") 
          for x in xrange(size) ])

DEFAULT_REVISIONS = [
            ('initial check-in', {
                "test.dat": random_data(1024),
                "test_dir/another_file.txt": "This is a test text file\nWith a few lines\nand no newline at the end..." }),
            ('second commit', {
                "new file with space.exe": random_data(8096),
                "test_dir/another_file.txt": "New first line!\nThis is a test text file\nWith a few lines\nand a newline at the end!\n" })]

class RepoTestManager(object):
    """Mocked up repository for testing as a context manager"""
    def __init__(self, path=None, bin=None, revisions=None, clone=None):
        self.path = path or self.make_path()
        self.bin = bin
        self.revisions = revisions if revisions is not None else DEFAULT_REVISIONS
        self.clone = clone

    def __enter__(self):
        """Initialise the path as git repository and
        put the dummy hierarcy and changes in there"""
        assert not os.path.exists(self.path)
        
        # create and initialise the repository
        os.makedirs(self.path)

        # if we are a clone, clone, else git init the empty dir
        if self.clone:
            r = self.run('{0.bin} clone "{0.clone}" .'.format(self))
        else:
            r = self.run('{0.bin} init'.format(self))
        assert r.status_code==0, "could not {0.bin} init {0.path}: {1.std_out} {1.std_err}".format(self,r)

        # commit the changesets
        for comment, files in self.revisions:
            self.commit_changes(comment, files)

        return self

    def __exit__(self, *args, **kwargs):
        r = self.run('rm -rf "{0.path}"'.format(self))
        assert r.status_code==0, "could not remove {0.bin} directory {0.path}: {1.std_out} {1.std_err}".format(self, r)

    def run(self, command, cwd=None):
        return envoy.run(command, cwd=cwd or self.path)

    def make_path(self):
        """if a path is not passed in, this method makes a temporary path (but doesn't create it)"""
        return os.path.join("/",tempfile.gettempprefix(),str(uuid.uuid4()))

    def write_file(self, relpath, contents):
        """Create a file name relpath with contents in the repo"""
        filepath = os.path.join(self.path, relpath)
        dirname = os.path.dirname(filepath)

        # make directory if it doesnt exist
        if not os.path.exists(dirname):
            os.makedirs(dirname)

        # write the file
        with open(filepath, 'wb') as fh:
            fh.write(contents)

    def write_files(self, files):
        """Pass a hash of {filename:contents} to make them"""
        for filename, contents in files.iteritems():
            self.write_file(filename, contents)

    def commit(self, message):
        r = self.run('{0.bin} commit -m "{1}"'.format(self, message))
        assert r.status_code==0, "could not {0.bin} commit '{1}': {2.std_out} {2.std_err}".format(self, message, r)

    def commit_changes(self, message, files):
        """Make changes and then commit with message"""
        self.write_files(files) # write each file
        self.add(files.keys())  # list of files to hg add
        self.commit(message)    # commit them

    def add(self,files):
        """hg add each file in files. List of relpaths."""
        command = '{0.bin} add '.format(self) + " ".join(
                      ['"'+str(fpath)+'"' for fpath in files])
        r = self.run(command)
        assert r.status_code==0, "could not {0}: {1.std_out} {1.std_err}".format(command, r) 

    def log(self):
        r = self.run('{0.bin} log'.format(self))
        assert r.status_code==0, "could not {0.bin} log: {1.std_out} {1.std_err}".format(self, r)
        return r.std_out
